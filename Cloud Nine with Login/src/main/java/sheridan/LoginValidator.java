package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	final static int MIN_NUMBER_OF_CHARACTERS = 6;
	
	public static boolean isValidLoginName( String loginName ) {
		boolean isAlphaNumeric = !Pattern.compile("[^a-zA-Z0-9]").matcher(loginName).find();
		boolean hasLetterFirst = !Pattern.compile("^[0-9]").matcher(loginName).find();
		boolean hasAppropriateLength = loginName.length() >= MIN_NUMBER_OF_CHARACTERS;
		return isAlphaNumeric && hasLetterFirst && hasAppropriateLength;
	}
}

package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Ramses28" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionAlhpaNoNumFirst( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "12345678ABC" ) );
	}
	
	@Test
	public void testIsValidLoginException2AlhpaNoNumFirst( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "A2345678ABC.%4" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryInAlhpaNoNumFirst( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "A1234567" ) );
	}
	

	@Test
	public void testIsValidLoginBoundaryOutAlhpaNoNumFirst( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "1ABCDEFG" ) );
	}
	
	@Test
	public void testIsValidLoginExceptionAlphaSix( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "" ) );
	}
	
	@Test
	public void testIsValidLoginException2AlphaSix( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "A28ABC.%4" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryInAlphaSix( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "A12345" ) );
	}
	

	@Test
	public void testIsValidLoginBoundaryOutAlphaSix( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "A1234" ) );
	}
}
